 //
//  main.cpp
//  pantherascript
//
//  Created by hmueller on 2017-09-27.
//  Copyright © 2017 hmueller. All rights reserved.
//

#include <string>
#include <iostream>

#include "../include/AST.h"
#include "../include/parser.h"
#include "../include/bytecode.h"
#include <functional>

template<typename Ret, typename... Args>
Ret callFunction(const std::string& name, Args... args) {
	std::function<Ret(Args...)> func = getByAddress<Ret(*)(Args...)>(name.c_str());
	return func(args...);
}

PSEXPORT double dub(double val) {
	return val * 2;
}

int main(int argc, const char * argv[]) {    
    Parser p;
    std::string cmd;
	std::cout << "> ";
    std::getline(std::cin, cmd);
    
    while(cmd != ".quit") {
        p.source(cmd);
        
        try {
            auto expr = p.parseExpression();
            while(expr != nullptr) {
                switch(expr->getType()){
                    case ExpressionType::decl_var: {
                        auto declVar = static_cast<const VariableDeclarationExpression*>(expr.get());
                        declVar->debugDump();
                        break;
                    }
                    case ExpressionType::number: {
                        auto numVar = static_cast<const NumberExpression*>(expr.get());
                        numVar->debugDump();
                        std::cout << '\n';
                        break;
                    }
                    case ExpressionType::string: {
                        auto stringVar = static_cast<const StringExpression*>(expr.get());
                        stringVar->debugDump();
                        std::cout << '\n';
                        break;
                    }
                    case ExpressionType::boolean: {
                        auto boolVar = static_cast<const BooleanExpression*>(expr.get());
                        boolVar->debugDump();
                        std::cout << '\n';
                        break;
                    }
                    case ExpressionType::identifier: {
                        auto id = static_cast<const IdentifierExpression*>(expr.get());
                        id->debugDump();
                        std::cout << '\n';
                        break;
                    }
                    case ExpressionType::asmt: {
                        auto id = static_cast<const VariableAssignmentExpression*>(expr.get());
                        id->debugDump();
                        std::cout << '\n';
                        break;
                    }
                    case ExpressionType::decl_func: {
                        auto func = static_cast<const FnProtoExpression*>(expr.get());
                        func->debugDump();
                        std::cout << '\n';
                        break;
                    }
                    default:
                        break;
                }
                
                expr = p.parseExpression();
            }
        }
        catch(const std::exception &e){
            LINE_ERROR(p.getLine(), p.tokenPosition(), "Error: " + std::string(e.what()));
            cmd.clear();
			std::cout << "\n> ";
            std::getline(std::cin, cmd);
            continue;
        }
        
        cmd.clear();
		std::cout << "> ";
        std::getline(std::cin, cmd);
    }
    
    return 0;
}
