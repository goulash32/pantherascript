//
//  token.h
//  pantherascript
//
//  Created by hmueller on 2017-09-27.
//  Copyright © 2017 hmueller. All rights reserved.
//

#ifndef token_h
#define token_h

enum class TokenType {
    eof         = -1,
    
    identifier  = -2,
    
    // declarations
    decl_var    = -3, // var
    decl_func   = -4, // func
    
    // literal
    number      = -5,
    string      = -6,
    boolean     = -7,
    
    // function prototype & definition
    func_proto  = -8, // func fName(arg0, arg1, ...);
    func_def	= -9, // func_proto { func_body }
    
    // operators
    op_un       = -10, // [!]x
    op_bin      = -11, // x [+] y, x [*] y, etc.
    op_tern     = -12, // x [?] y : z
    op_asmt     = -13, // x [=] y
    op_comp     = -14, // x == y
    
    // schema
    schema_def  = -15, // schema sName { schema_body }
    
    // lambda
    lambda_def  = -16, // $(arg0, arg1, ...) { func_body }
    
    // conditionals & loops
    cond_if     = -17,
    cond_while  = -18,
    cond_do     = -19,
    cond_for    = -20,
};

#endif /* token_h */
