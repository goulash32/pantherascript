//
//  symboltable.h
//  pantherascript
//
//  Created by hmueller on 2017-09-28.
//  Copyright © 2017 hmueller. All rights reserved.
//

#ifndef symboltable_h
#define symboltable_h

#include <unordered_map>

class Symbol;

class SymbolTable {
public:
    void addSymbol(const std::string& symbolName,
                   std::unique_ptr<Symbol> value) {
        m_symbols[symbolName] = std::move(value);
    }
    
    bool hasSymbol(const std::string& symbolName) const {
        return m_symbols.find(symbolName) != m_symbols.end();
    }
    
    std::shared_ptr<Symbol> getSymbol(const std::string &symbolName) const {
        auto sym = m_symbols.find(symbolName);
        
        if(sym == m_symbols.end()) {
            return nullptr;
        }
        
        return sym->second;
    }
    
private:
    std::unordered_map<std::string, std::shared_ptr<Symbol>> m_symbols;
};

#endif /* symboltable_h */
