//
//  parser.h
//  pantherascript
//
//  Created by hmueller on 2017-09-27.
//  Copyright © 2017 hmueller. All rights reserved.
//

#ifndef parser_h
#define parser_h

#include <unordered_map>
#include <queue>
#include <string>
#include <sstream>
#include <memory>

#include "AST.h"
#include "token.h"
#include "symbol.h"
#include "symboltable.h"

#include "../deps/to_floating_point/to_floating_point.hpp"

#include "bytecode.h"

#define LINE_ERROR(line, column, error) std::stringstream err; \
                                        err << line << ":" << column << ' ' << error; \
                                        std::cout << err.str() << '\n'

#define UP_IDEXPR(name)         std::make_unique<IdentifierExpression>(name)
#define UP_NUMEXPR(number)      std::make_unique<NumberExpression>(number)
#define UP_STREXPR(string)      std::make_unique<StringExpression>(string)
#define UP_BOOLEXPR(flag)       std::make_unique<BooleanExpression>(flag)
#define UP_VDECLEXPR(id, expr)  std::make_unique<VariableDeclarationExpression>(std::move(id), std::move(expr))
#define UP_VASMTEXPR(id, expr)  std::make_unique<VariableAssignmentExpression>(std::move(id), std::move(expr))
#define UP_FDECLEXPR(id, args)  std::make_unique<FnProtoExpression>(std::move(id), std::move(args))

using ExpressionQueue = std::deque<Expression>;

class Parser {
public:
    void source(const std::string &source) {
        m_source = source;
        m_cursor = 0;
        m_line = 1;
        m_column = 1;
    }
    
    void source(std::string &&source) {
        m_source = std::move(source);
        m_cursor = 0;
        m_line = 1;
        m_column = 1;
    }
    
    const std::string &currentToken() {
        return m_curToken;
    }
    
    int getToken() {
        m_prevColumn = m_column;
        
        m_curToken = "";
        
        // we've reached the end of the file!
        if(isEOF()) {
            return (int)TokenType::eof;
        }
        
        char curChar = m_source[m_cursor];
        
        if(curChar == '\n') {
            return handleNewline();
        }
        
        if(curChar == ';') {
            getChar();
            consumeSpaces();
            return getToken();
        }
        
        if(curChar == '\'' || curChar == '"') {
            char iChar = curChar;
            curChar = getChar();
            
            // do we even char?
            while(!isEOF() && curChar != iChar) {
                if(curChar == '\\') {
                    handleEscape();
                    curChar = getChar();
                    continue;
                }
                
                m_curToken += curChar;
                curChar = getChar();
            }
            
            if(isEOF()) {
                std::stringstream err;
                err << "Invalid string, expected '" << iChar << "'.";
                throw std::runtime_error(err.str());
            }
            
            getChar();
            consumeSpaces();
            return (int)TokenType::string;
        }
    
        if(isAlpha(curChar) || curChar == '_') {
            do {
                m_curToken += curChar;
                curChar = getChar();
            } while(isAlphaNumeric(curChar));
            
            consumeSpaces();
            
            if(curChar == '(') {
                // advance to start of expression
                getChar();
                consumeSpaces();
                
                if(m_curToken == "if") {
                    return (int)TokenType::cond_if;
                }
                
                if(m_curToken == "while") {
                    return (int)TokenType::cond_while;
                }
                
                if(m_curToken == "do") {
                    return (int)TokenType::cond_do;
                }
                
                if(m_curToken == "for") {
                    return (int)TokenType::cond_for;
                }
                
                return (int)TokenType::func_proto;
            }
            
            if(m_curToken == "var") {
                return (int)TokenType::decl_var;
            }
            
            if(m_curToken == "func") {
                return (int)TokenType::decl_func;
            }
            
            if(m_curToken == "true" || m_curToken == "false") {
                return (int)TokenType::boolean;
            }
            
            return (int)TokenType::identifier;
        }
        
        bool decSet = false;
        if(isNumber(curChar) || (decSet = curChar == '.')) {
            if(decSet) {
                if(!isNumber(nextChar())) {
                    throw std::runtime_error("Expected number after '.'.");
                }
            }
            
            do {
                m_curToken += curChar;
                curChar = getChar();
                
                if(isEOF()) break;
                
                // only one decimal allowed per number!
                if(curChar == '.') {
                    if(decSet) {
                        throw std::runtime_error("Malformed number. Multiple decimal points.");
                    }
                    
                    decSet = true;
                }
            } while(isNumber(curChar) || curChar == '.');
            
            // if(curChar != ' ' && curChar != ';' &&
            // curChar != ',' && curChar != ')' && !isEOF()) {
            if(!expects(curChar, {' ', ';', ',', ')'}) && !isEOF()) {
                throw std::runtime_error("Unexpected character '" + std::string(1, curChar) + "' in number.");
            }
            
            if(curChar == ')') {
                return (int)TokenType::number;
            }
            
            getChar();
            consumeSpaces();
            return (int)TokenType::number;
        }
        
        if(curChar == '='){
            m_curToken += curChar;
            getChar();
            
            if(!isEOF() && m_source[m_cursor] == '=') {
                m_curToken += curChar;
                getChar();
                consumeSpaces();
                return (int)TokenType::op_comp;
            }
            
            consumeSpaces();
            return (int)TokenType::op_asmt;
        }
        
       curChar = m_source[m_cursor];
        
        if(getChar() >= 0) {
            consumeSpaces();
            return curChar;
        }
        
        return (int)TokenType::eof;
    }
    
    int peekToken(std::string &token) {
        unsigned long saveCursor = m_cursor;
        auto tok = getToken();
        token = m_curToken;
        m_cursor = saveCursor;
        return tok;
    }
    
    std::unique_ptr<Expression> parseExpression() {
        try {
            int token;
            token = getToken();
            
            if(expects(token, {';', ','})) {
                token = getToken();
            }
            
            while(token != (int)TokenType::eof) {
                switch(token) {
                    case (int)TokenType::identifier: {
                        consumeSpaces();
                        auto name = currentToken();
                        
                        auto curChar = m_source[m_cursor];
                        // other expressions need to check for ')', do not consume
                        if(expects(curChar, ')') || isEOF()) {
                            return UP_IDEXPR(name);
                        }
                        
                        if(expects(curChar, {',', ';'})) {
                            getChar();
                            consumeSpaces();
                            return UP_IDEXPR(name);
                        }
                        
                        token = getToken();
                        if(token == (int)TokenType::op_asmt) {
                            auto expr = parseExpression();
                            auto id = UP_IDEXPR(name);
                            return UP_VASMTEXPR(id, expr);
                        }
                    }
                    case (int)TokenType::number: {
                        double num;
                        num = stringToNumber<double>(currentToken());
                        return UP_NUMEXPR(num);
                    }
                    case (int)TokenType::string: {
                        return UP_STREXPR(currentToken());
                    }
                    case (int)TokenType::boolean: {
                        return UP_BOOLEXPR(currentToken() == "true");
                    }
                    case (int)TokenType::decl_var: {
                        token = getToken();
                        auto name = currentToken();
                        
                        if(name.empty()) {
                            throw std::runtime_error("Expected identifier in variable declaration.");
                        }
                        
                        if(!expects(token, (int)TokenType::identifier)) {
                            throw std::runtime_error("Invalid identifier.");
                        }
                        
                        if(isExtantIdentifier(name)) {
                            throw std::runtime_error("Redeclaration of '" + name + "'.");
                        }
                        
                        token = getToken();
                        auto id = UP_IDEXPR(name);
                        std::unique_ptr<VariableDeclarationExpression> ret;
                        
                        if(token == (int)TokenType::op_asmt) {
                            token = getToken();
                            
                            if(token == (int)TokenType::number) {
                                double val = stringToNumber<double>(currentToken());
                                auto num = UP_NUMEXPR(val);
                                ret = UP_VDECLEXPR(id, num);
                            } else if(token == (int)TokenType::string) {
                                auto str = UP_STREXPR(currentToken());
                                ret = UP_VDECLEXPR(id, str);
                            } else if(token == (int)TokenType::boolean) {
                                auto bl = UP_BOOLEXPR(currentToken() == "true");
                                ret = UP_VDECLEXPR(id, bl);
                            } else if(token == (int)TokenType::identifier) {
                                if(!isExtantIdentifier(currentToken())) {
                                    throw std::runtime_error("Invalid identifier '" + currentToken() + "'.");
                                }
                                
                                auto oId = UP_IDEXPR(currentToken());
                                ret = UP_VDECLEXPR(id, oId);
                            }
                        } else { // declaration without assignment
                            ret = UP_VDECLEXPR(id, nullptr);
                        }
                        
                        m_identifierToId[name] = m_currentId++;
                        
						return ret;
                    }
                    case (int)TokenType::decl_func: {
                        token = getToken();
                        
                        if(!expects(token, (int)TokenType::func_proto)) {
                            throw std::runtime_error("Expected identifer and argument list in function declaration.");
                        }
                        
                        auto name = currentToken();
                        auto id = UP_IDEXPR(name);
                        
                        std::vector<std::unique_ptr<Expression>> args;
                        while(!isEOF() && m_source[m_cursor] != ')') {
                            auto expr = parseExpression();
                            args.push_back(std::move(expr));
                        }
                        
                        if(isEOF()) {
                            throw std::runtime_error("Expected ')' after argument list.");
                        }
                        
                        getChar();
                        consumeSpaces();
                        
                        std::cout << '\n';
                        return UP_FDECLEXPR(id, args);
                    }
                    default:
                        throw std::runtime_error("Unexpected token '" + (m_curToken.empty() ? std::string(1, m_source[m_cursor]) : m_curToken) + "'\n");
                }
                
                token = getToken();
            }
        } catch(const std::runtime_error& e) {
            throw e;
        }
        
        return nullptr;
    }
    
    unsigned int getLine() const {
        return m_line;
    }
    
    unsigned int getColumn() const {
        return m_column;
    }
    
    unsigned int tokenPosition() const {
        return m_prevColumn;
    }
    
private:
    std::unique_ptr<SymbolTable>                    m_globalSymbolTable;
    std::string                                     m_source;
    unsigned long                                   m_cursor = 0;
    
    std::string                                     m_curToken;
    
    std::unordered_map<std::string, unsigned long>  m_identifierToId;
    unsigned long                                   m_currentId = 0;
    
    // position tracking
    unsigned int                                    m_line = 1;
    unsigned int                                    m_column = 1;
    unsigned int                                    m_prevColumn = 1;
    
    // character helpers //////////////////////////////////////
    inline bool isSpace(char character) const {
        return character == ' ';
    }
    
    inline bool isNumber(char character) const {
        return character >= 48 && character <= 57;
    }
    
    inline bool isAlpha(char character) const {
        return character >= 65 && character <= 122;
    }
    
    inline bool isAlphaNumeric(char character) const {
        return isNumber(character) || isAlpha(character);
    }
    
    inline bool isEOF() const {
        return m_cursor >= m_source.size();
    }
    
    int expects(int token, int exp) {
        if(token != exp) {
            return 0;
        }
        
        return token;
    }
    
    int expects(int token, std::vector<int> &&exps) {
        for(auto tok : exps) {
            if(tok == token) {
                return tok;
            }
        }
        
        return 0;
    }
    
    // todo: expand. right now only checks that string is not empty and first character is alpha.
    inline bool isValidIdentifier(const std::string &id) const {
        if(id.empty()) {
            return false;
        }
        
        return isAlpha(id[0]);
    }
    
    inline bool isExtantIdentifier(const std::string &id) const {
        return m_identifierToId.find(id) != m_identifierToId.end();
    }
    
    inline void consumeSpaces() {
        if(isEOF()) return;
        
        while(isSpace(m_source[m_cursor])) {
            getChar();
        }
    }
    
    int handleNewline() {
        getChar();
        ++m_line;
        m_column = 1;
        consumeSpaces();
        return getToken();
    }
        
    int handleEscape() {
        auto curChar = m_source[m_cursor];
        
        if(curChar == '\\') {
            char escape = getChar();
            
            if(escape == '\"' || escape == '\'') {
                m_curToken += escape;
                return ' ';
            }
            
            if(escape == '\\') {
                m_curToken += escape;
                return ' ';
            }
            
            if(escape == '\n') {
                m_curToken += '\n';
                return ' ';
            }
        }
        
        throw std::runtime_error("Unrecognized escape sequence.");
    }

    inline char getChar() {
        ++m_cursor;
        ++m_column;
        
        if(isEOF()) {
            return -1;
        }
        
        return m_source[m_cursor];
    }
    
    char nextChar() {
        auto tempCursor = m_cursor + 1;

        if(tempCursor >= m_source.size()) {
            return (char)TokenType::eof;
        }
        
        while(m_source[tempCursor] == ' ') {
            ++tempCursor;
            if(tempCursor >= m_source.size()) {
                return (char)TokenType::eof;
            }
        }

        return m_source[tempCursor];
    }
    
    template<typename T>
    T stringToNumber(const std::string &numStr) {
        try {
            return to_floating_point<double>(numStr);
        }
        catch(const std::exception& e) {
            throw std::runtime_error("Token is not valid number: " + std::string(e.what()));
        }
    }
    ////////////////////////////////////////////////////////////
};

#endif /* parser_h */
