//
//  bytecode.h
//  pantherascript
//
//  Created by hmueller on 2017-09-28.
//  Copyright © 2017 hmueller. All rights reserved.
//

#ifndef bytecode_h
#define bytecode_h

#include <string>

#if defined __unix__ || __APPLE__
#include <dlfcn.h>
#include <memory>

#include "symbol.h"
#include "symboltable.h"

#define PSEXPORT extern "C"

template<typename T>
T getByAddress(const std::string &symbolName) {
    auto handle = dlopen(nullptr, RTLD_LAZY);
    auto ptr = dlsym(handle, symbolName.c_str());
    return reinterpret_cast<T>(ptr);
}

class Builder {
public:
    Builder() {
        m_symbolTable = std::make_unique<SymbolTable>();
    }
    
    const SymbolTable *getGlobalSymbolTable() const {
        return m_symbolTable.get();
    }
    
private:
    std::unique_ptr<SymbolTable> m_symbolTable;
};

struct Header {
    
};

#endif // __unix__ || __APPLE__
#if defined _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#define PSEXPORT extern "C" __declspec(dllexport)

template<typename T>
T getByAddress(const std::string &symbolName) {
	auto handle = GetModuleHandle(nullptr);
	auto ptr = GetProcAddress(handle, symbolName.c_str());
	return reinterpret_cast<T>(ptr);
}

#endif // _WIN32

#endif /* bytecode_h */
