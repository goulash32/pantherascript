//
//  AST.h
//  pantherascript
//
//  Created by hmueller on 2017-09-27.
//  Copyright © 2017 hmueller. All rights reserved.
//

#ifndef AST_h
#define AST_h

#include <vector>
#include <memory>

enum class ExpressionType {
    identifier,
    asmt,
    number,
    string,
    boolean,
    decl_var,
    decl_func,
    def_func
};

class Expression {
public:
    virtual ~Expression() {};
    
    ExpressionType getType() const {
        return m_type;
    }
    
    virtual void debugDump() const {}
    
protected:
    ExpressionType m_type;
};

// identifier
class IdentifierExpression : public Expression {
public:
    IdentifierExpression(const std::string &name)
    : m_name(name) {
        m_type = ExpressionType::identifier;
    }
    
    void debugDump() const {
        std::cout << m_name;    
    }
    
private:
    std::string m_name;
};

// value expression - base class of basic types
class ValueExpression : public Expression {
public:
    virtual ~ValueExpression() {};
};

// numeric literal expression
class NumberExpression : public ValueExpression {
public:
    NumberExpression(double value) : m_value(value) {
        m_type = ExpressionType::number;
    }
    
    const double &getValue() const {
        return m_value;
    }
    
    void setValue(double value) {
        m_value = value;
    }
    
    void debugDump() const {
        std::cout << "number(" << m_value << ')';
    }
    
private:
    double m_value;
};

// string literal expression
class StringExpression : public ValueExpression {
public:
    StringExpression(const std::string &body) : m_body(body) {
        m_type = ExpressionType::string;
    }
    
    const std::string &getValue() const {
        return m_body;
    }
    
    void setValue(const std::string &body) {
        m_body = body;
    }
    
    void debugDump() const {
        std::cout << "string('" << m_body << "')";
    }
    
private:
    std::string m_body;
};

// boolean expression
class BooleanExpression : public ValueExpression {
public:
    BooleanExpression(bool flag) : m_flag(flag) {
        m_type = ExpressionType::boolean;
    }
    
    bool getValue() const {
        return m_flag;
    }
    
    void setValue(bool flag) {
        m_flag = flag;
    }
    
    void debugDump() const {
        std::cout << "boolean(" << (m_flag ? "true" : "false") << ')';
    }
    
private:
    bool m_flag;
};

// variable declaration expression
class VariableDeclarationExpression : public Expression {
public:
    VariableDeclarationExpression(std::unique_ptr<IdentifierExpression> name,
                                  std::unique_ptr<Expression> initVal)
    : m_name(std::move(name)), m_value(std::move(initVal)) {
        m_type = ExpressionType::decl_var;
    }
    
    void debugDump() const {
        m_name->debugDump();
        std::cout << ":\t\t";
        if(m_value) {
            m_value->debugDump();
        } else {
            std::cout << "notype";
        }
        std::cout << '\n';
    }
    
private:
    std::unique_ptr<IdentifierExpression>   m_name;
    std::unique_ptr<Expression>             m_value;
};

// function prototype declaration
class FnProtoExpression : public Expression {
public:
    FnProtoExpression(std::unique_ptr<IdentifierExpression> name,
                     std::vector<std::unique_ptr<Expression>> args)
    : m_name(std::move(name)), m_args(std::move(args)) {
        m_type = ExpressionType::decl_func;
    }
    
    void debugDump() const {
        m_name->debugDump();
        std::cout << '(';
        for(int i = 0; i < m_args.size(); ++i) {
            m_args[i]->debugDump();
            if(i < m_args.size() - 1) {
                std::cout << ", ";
            }
        }
        std::cout << ')';
    }
    
private:
    std::unique_ptr<IdentifierExpression>       m_name;
    std::vector<std::unique_ptr<Expression>>    m_args;
};

// function definition
class FnDefExpression : public Expression {
public:
    FnDefExpression(std::unique_ptr<FnProtoExpression> proto,
                    std::unique_ptr<Expression> body)
    : m_proto(std::move(proto)), m_body(std::move(body)) {}
    
    void debugDump() const {
        
    }
    
private:
    std::unique_ptr<FnProtoExpression>          m_proto;
    std::unique_ptr<Expression>                 m_body;
};

class VariableAssignmentExpression : public Expression {
public:
    VariableAssignmentExpression(std::unique_ptr<IdentifierExpression> id,
                                 std::unique_ptr<Expression> rhs)
    : m_id(std::move(id)), m_rhs(std::move(rhs)) {
        m_type = ExpressionType::asmt;
    }
    
    void debugDump() const {
        m_id->debugDump();
        std::cout << ":\t\t";
        m_rhs->debugDump();
    }
    
private:
    std::unique_ptr<IdentifierExpression>   m_id;
    std::unique_ptr<Expression>             m_rhs;
};

#endif /* AST_h */
