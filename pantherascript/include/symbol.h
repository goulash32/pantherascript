//
//  symbol.h
//  pantherascript
//
//  Created by hmueller on 2017-10-01.
//  Copyright © 2017 hmueller. All rights reserved.
//

#ifndef symbol_h
#define symbol_h

#include <string>
#include <memory>

enum class BCSymbolType {
    exit,
    identifier,
    asmt,
    lit_num,
    lit_str,
    lit_bool,
    add,
    sub,
    mul,
    div
};

class Symbol {
public:
    virtual ~Symbol() {};
    
    BCSymbolType getType() const {
        return m_symbolType;
    }
    char getCode() const {
        return m_code;
    }
    
protected:
    BCSymbolType    m_symbolType;
    char            m_code;
};

class IdentifierSymbol : public Symbol {
public:
    IdentifierSymbol(const std::string &name) : m_name(name) {
        m_symbolType = BCSymbolType::identifier;
    }
    const std::string &getName() const {
        return m_name;
    }
    
private:
    std::string m_name;
};

class AssignmentSymbol : public Symbol {
protected:
    std::shared_ptr<IdentifierSymbol>   m_identifier;
    std::shared_ptr<Symbol>             m_expression;
};


#endif /* symbol_h */
